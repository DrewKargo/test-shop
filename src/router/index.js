import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import ModalProduct from '@/components/modal/ModalProduct.vue'
import CatalogView from '@/views/CatalogView.vue'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomeView,
            children: [
                {
                    path: 'product',
                    component: ModalProduct
                }
            ]
        },
        {
            path: '/:category',
            name: 'catalog',
            component: CatalogView
        }
    ]
})

export default router
