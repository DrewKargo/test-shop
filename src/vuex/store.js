import { createStore } from 'vuex'

const store = createStore({
    state: {
        modalContent: null
    }
})

export default store
