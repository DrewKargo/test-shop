import './assets/scss/main.scss'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './vuex/store'
import Vuelidate from 'vuelidate'

const app = createApp(App)

app.use(router)

app.use(store)

app.use(Vuelidate)

app.mount('#app')
